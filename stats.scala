// spark-shell --executor-memory 14g --executor-cores 7 --num-executors 8

// screen spark-shell --master yarn --driver-cores 25 --driver-memory=200g --executor-memory 39g --executor-cores 5 --num-executors 25 --conf "spark.kryoserializer.buffer.max=1024m" --conf "spark.yarn.executor.memoryOverhead=7000" --conf "spark.driver.maxResultSize=0" --conf "spark.sql.shuffle.partitions=150"  --conf "spark.memory.fraction=0.8" --conf "spark.speculation=true" --conf "spark.speculation.quantile=0.99"

import org.apache.spark.sql.functions._

// ID, Pairs and Clusters by country

val commonElements = udf((s:Seq[String],v:Seq[String]) => s.intersect(v))

val toSet = udf((s:Seq[String]) => s.toSet.toSeq)


// root
//  |-- uiid1: string (nullable = true)
//  |-- uiidType1: string (nullable = true)
//  |-- deviceType1: string (nullable = true)
//  |-- ip1: array (nullable = true)
//  |    |-- element: binary (containsNull = true)
//  |-- country1: array (nullable = true)
//  |    |-- element: string (containsNull = true)
//  |-- uiid2: string (nullable = true)
//  |-- uiidType2: string (nullable = true)
//  |-- deviceType2: string (nullable = true)
//  |-- ip2: array (nullable = true)
//  |    |-- element: binary (containsNull = true)
//  |-- country2: array (nullable = true)
//  |    |-- element: string (containsNull = true)
//  |-- score: double (nullable = true)
// val matchedPairs = spark.read.parquet("s3://adbrain-graph-prod/private/adbrain/archive/agg-msresults/3/2016-11-26_2017-02-26/part-r-00411-8d2c6340-7538-47c3-b77d-2429b2a6987e.gz.parquet")
val matchedPairs = spark.read.parquet("s3://adbrain-graph-prod/private/adbrain/archive/agg-msresults/3/2016-11-26_2017-02-26/*.parquet")
		.filter($"score">=0.1).select($"uiid1",toSet($"country1") as "country1",$"uiid2",toSet($"country2") as "country2")



// Q1: how many pairs per country?
val pairs = matchedPairs.withColumn("commonCountries", commonElements($"country1", $"country2"))
		.withColumn("commonCountries", explode($"commonCountries"))

val pairsCount = pairs.groupBy($"commonCountries").agg(count("commonCountries")).toDF("country", "count")
	.sort(desc("count"))

pairsCount.coalesce(1).write.csv("s3://adbrain-dev/george/CP-576/pairsPerCountry.csv")

// Q2: how many ids by country
val idCountry = matchedPairs.select($"uiid1" as "uiid",  $"country1" as "country")
		.union(matchedPairs.select($"uiid2" as "uiid",  $"country2" as "country"))
		.distinct.withColumn("country", explode($"country")).cache()



val countriesDist = idCountry.groupBy($"country").agg(countDistinct($"uiid").as("count")).sort(desc("count"))

countriesDist.coalesce(1).write.csv("s3://adbrain-dev/george/CP-576/countriesDist.csv")

// root
//  |-- scv_id: string (nullable = true)
//  |-- uiid: string (nullable = true)
//  |-- uiidType: string (nullable = true)
//  |-- deviceType: string (nullable = true)
//  |-- isSeed: boolean (nullable = true)
// val components = spark.read.parquet("s3://adbrain-prod/scv/adbrain/2017-02-26-00-00-00/part-r-00381-ea711591-c102-44e1-a98b-0bee3e2fb650.gz.parquet")
val components = spark.read.parquet("s3://adbrain-prod/scv/adbrain/2017-02-26-00-00-00/*.parquet")

val cidsPerCountry = components.join(idCountry, "uiid").select($"scv_id", $"country").distinct
					.groupBy("country").count().sort(desc("count"))

val cidsCount = components.select($"scv_id").distinct.count
//== 739407731

cidsPerCountry.coalesce(1).write.csv("s3://adbrain-dev/george/CP-576/cidsPerCountry.csv")

// how many unique clusters in total?